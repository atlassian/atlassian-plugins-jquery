const path = require('path');
const getPackageMainFilePath = (name) => require.resolve(name);
const resolveTargetFromRoot = (target) => path.resolve(__dirname, '..', target);

const dependencies = [
    {
        npmPackage: 'jquery',
        relativeTarget: 'target/classes/jquery.js'
    },
    {
        npmPackage: 'jquery-migrate',
        relativeTarget: 'target/classes/jquery-migrate.js'
    },
];

const mappedDependencies = dependencies.map(({npmPackage, relativeTarget}) => {
    return {
        npmPackage,
        source: getPackageMainFilePath(npmPackage),
        target: resolveTargetFromRoot(relativeTarget)
    };
});

module.exports = mappedDependencies;
