const path = require('path');
const { promises: fs } = require('fs');
const mkdirp = require('mkdirp');
const dependencies = require('./dependencies');

async function main() {

    for (let {source, target} of dependencies) {
        await mkdirp(path.dirname(target));

        await fs.copyFile(source, target);
    }

    console.log(`Copied ${dependencies.length} file(s)`);
}

(async () => {
    try {
        await main();
    } catch (error) {
        console.log(error);
        console.log(error.stack);
        process.exit(1);
    }
})();
