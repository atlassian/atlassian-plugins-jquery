const $ = require('jquery');

describe('jquery.js', () => {
    describe('CONFSRVDEV-15328', () => {
        it('does not prefilter html', () => {
            const input = '<div/>';
            const result = $.htmlPrefilter(input);
            expect(result).toEqual(input);
        });
    });

    // JQUERY-8 - test that Event objects don't force reflows
    describe('JQUERY-8', () => {
        ['clientX', 'pageX', 'offsetX', 'which'].forEach(prop => {
            it(`uses getter for '${prop}' on events to avoid reflow until property accessed`, () => {
                const descriptor = Object.getOwnPropertyDescriptor($.Event.prototype, 'clientX')
                expect(descriptor).toBeDefined();
                expect(typeof descriptor.get).toEqual('function');
            });
        });
    });
});
