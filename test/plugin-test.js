const fs = require('fs');

describe('code copied', () => {
    const libs = require('../scripts/dependencies');

    libs.forEach(({ npmPackage, source, target }) => {
        it(`${npmPackage}: copied to target`, () => {
            const oldSrc = fs.readFileSync(source, 'utf8');
            const newSrc = fs.readFileSync(target, 'utf8');
            expect(newSrc).toEqual(oldSrc);
        });
    })
});
