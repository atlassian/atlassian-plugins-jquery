## jQuery Atlassian plugin 

It is a P2 maven plugin which wraps [jQuery][jq], and optionally [jQuery migrate plugin][jqm],
designed for the cross-product consumption.

## Branches

- `master` - jQuery 2.x (master is based on JQuery 2.x version and should only be updated to new versions of 2.x only)
- `3.x` - jQuery 3.x (3.x branch uses JQuery 3.x and should only be updated to new versions of 3.x only)
- `1.x` - jQuery 1.x (1.x branch uses JQuery 1.x and should only be updated to new versions of 1.x only)
  - Considered "Unsupported"

## Usage

This project provides a few `web-resource` that you can depend upon in yours.

Be aware that your product might already depend on jQuery through this plugin.
Your product may configure and expose jQuery via a product-specific web-resource or module.
If so, you should use your product's `web-resource` keys instead of these.

| `web-resource` key | [jQuery library][jq] | [jQuery migrate plugin][jqm] | [`jQuery.browser`][jqb] |
| :---  |  :----:  |  :----:  |  :----:  |
|`com.atlassian.plugins.jquery:jquery`| ✅ | ✅ | ✅ |
|`com.atlassian.plugins.jquery:jquery-lib`| ✅ | ❌ | ❌ |
|`com.atlassian.plugins.jquery:jquery-browser`| ✅ | ❌ | ✅ |
|`com.atlassian.plugins.jquery:jquery-migrate`| ✅ | ✅ | ✅ |

By default, [jQuery migrate][jqm] will not log any warnings to console. If you want those,
you will need to enable the dark feature `plugins.jquery.migrate.logging` via UI or with a system property.

## Issues

Issues should be raised on [the JQUERY ecosystem project][5].

## Contributing

The source code for `jquery` and `jquery-migrate` are managed via Node.
We apply patches to their source via [the `patch-package` Node module][patch-package].
The patches we apply can be found in the `patches/` directory.

To make further modifications to the library:

0. Run `npm install`.
   This will run `patch-package` via a `postinstall` step, applying our existing patches.
0. Edit the files in `node_modules/jquery/` and `node_modules/jquery-migrate` directly.
0. Run `npx patch-package jquery jquery-migrate`.
   This will update the files in `patches/` directory.
0. Commit the updated patch files.

To verify the changes work in the atlassian plugin:

0. Run `mvn verify` to ensure all compilation steps work.
0. Run `mvn amps:run -Dproduct=refapp` to test the plugin in a running product.
   You can substitute `refapp` for another product (e.g., `jira`, `confluence`, `bitbucket`, etc.)
0. Ensure both the `.js` and `-min.js` variants of the files contain the patches
   and valid license code comments at the top.
   `mvn amps:run` will start a product with dev mode off; `mvn amps:debug` with it on.

To release a new version:

0. The license for new versions should be added in atlassian-licenses repository
   for [jQuery itself][4] and [this maven plugin][3] accordingly.
0. Go to [the jquery plugin Bamboo build][build].
0. Run the build for the appropriate branch of this repo.
0. Run the manual release step after tests pass.


[jq]: http://jquery.com
[jqm]: https://github.com/jquery/jquery-migrate
[jqb]: https://github.com/gabceb/jquery-browser-plugin
[3]: https://stash.atlassian.com/projects/CP/repos/atlassian-licenses/browse/com/atlassian/plugins/jquery
[4]: https://stash.atlassian.com/projects/CP/repos/atlassian-licenses/browse/other/jqueryjs
[5]: https://ecosystem.atlassian.net/browse/JQUERY
[build]: https://ecosystem-bamboo.internal.atlassian.com/browse/JQUERY
[patch-package]: https://www.npmjs.com/package/patch-package
