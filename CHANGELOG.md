# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [2.2.4.15]
### Changed
- [DCA11Y-861] `jquery.browser` is now managed as a copy from the source repository, with Atlassian modifications kept
  separately
- [DCA11Y-861] The build artifact contains FE manifests and associations, for generating SBOMs in the products

## [2.2.4.14]
### Changed
- [CONFSRVDEV-30703] To disable jquery-migrate, the `plugins.jquery.migrate.load.disabled` dark feature needs to be set

## [2.2.4.13]
### Removed
- [DCPL-1002] Removed `IsDarkFeatureEnabledUrlReadingCondition` to keep compatibility with Platform 7 along with old ones

## [2.2.4.12]
### Added
- [JQUERY-32] Added new condition `IsDarkFeatureEnabledUrlReadingCondition` to enable `jquery-migrate-logging` web-resource by checking dark feature flag `plugins.jquery.migrate.logging` (disabled by default)
- Added new web-resource `jquery-migrate-with-logging` which includes jquery, jquery-browser, jquery-migrate, and jquery-migrate-logging for convenience and to control dependency order

## [2.2.4.11]
### Fixed
- Applied jQuery.event.fix patch from https://github.com/jquery/jquery/pull/2860 to avoid force reflow on event creation.

### Changed
- [JQUERY-9] Load jquery through npm and apply patches via patch file. This ensures the security scanner works on jquery.

## [2.2.4.10]
### Fixed
- [CONFSRVDEV-15328] [CVE-2020-11022] [SNYK-JS-JQUERY-567880] [VULN-1133280] Applied 'htmlPrefilter' security fix from jQuery 3.5.0
- [CONFSRVDEV-15328] [CVE-2020-11023] [SNYK-JS-JQUERY-565129] [VULN-1133278] Applied workaround for `<option>` XSS vulnerability patched in jQuery 3.5.0
- Above patches drops support for IE9

## [2.2.4.9] - 2019-12-20
### Added
- [JQUERY-2] Added new `jquery-browser` web-resource to implement `jQuery.browser` object.

### Changed
- [JQUERY-2] Replaced `jQuery.browser` implementation in jquery-migrate with one from [jquery-browser-plugin](https://github.com/gabceb/jquery-browser-plugin). It can now detect IE 11 and Edge browsers.
- [JQUERY-3] AMPS and other package upgrades

## [2.2.4.8] - 2019-08-02
### Added
- Reverted removal of `jQuery.browser` from the jquery-migrate plugin (previously removed in 2.2.4.3).

## [2.2.4.7]
### Fixed
- [CVE-2019-11358] [JFE-28] [MNSTR-2847] [SNYK-JS-JQUERY-174006] [VULN-1133281] [Applied](https://bitbucket.org/atlassian/atlassian-plugins-jquery/commits/174ddda637eefa27359f78a7b77e44243d71e7d5) 'proto' security fix from jQuery 3.4.0

## [2.2.4.6]
### Changed
- Reconfigured `jquery-migrate-logging` to load automatically as soon as it's enabled (still disabled by default)

## [2.2.4.5]
### Changed
- Made warnings in jquery-migrate optional by importing new `jquery-migrate-logging` web-resource (disabled by default)

## [2.2.4.4]
### Changed
- Disabled `jQuery.migrateTrace` which prevents printing warnings in jquery-migrate

## [2.2.4.3]
### Removed
- Removed `jQuery.browser` from jquery-migrate

## [2.2.4.2]
### Fixed
- `jQuery.parseHTML` [patch](https://bitbucket.org/atlassian/atlassian-plugins-jquery/commits/b95ba9cd673cadb5c84bae9fa2445ff6dcecc12f) backported from 3.3.1
- [CVE-2015-9251] [SNYK-JQUERY-20150627] [VULN-1051355] `jQuery.ajaxPrefilter` [patch](https://bitbucket.org/atlassian/atlassian-plugins-jquery/commits/bd5d29b9c09563f185a737733fb0ece3d739dc81) backported from 3.3.1 
### Removed
- Removed `jQuery.Deferred.pipe` overrides and added `jQuery.curCSS` to jquery-migrate

## [2.2.4.1]
### Changed
- Build changes

### [2.2.4]
Initial 2.x release:
- jQuery 2.2.4
- jQuery Migrate 1.4.1


[Unreleased]: https://bitbucket.org/atlassian/atlassian-plugins-jquery/compare/master%0Drelease%2F2.2.4.15
[2.2.4.15]: https://bitbucket.org/atlassian/atlassian-plugins-jquery/compare/release%2F2.2.4.15%0Drelease%2F2.2.4.14
[2.2.4.14]: https://bitbucket.org/atlassian/atlassian-plugins-jquery/compare/release%2F2.2.4.14%0Drelease%2F2.2.4.13
[2.2.4.13]: https://bitbucket.org/atlassian/atlassian-plugins-jquery/compare/release%2F2.2.4.13%0Drelease%2F2.2.4.12
[2.2.4.12]: https://bitbucket.org/atlassian/atlassian-plugins-jquery/compare/release%2F2.2.4.12%0Drelease%2F2.2.4.11
[2.2.4.11]: https://bitbucket.org/atlassian/atlassian-plugins-jquery/compare/release%2F2.2.4.11%0Drelease%2F2.2.4.10
[2.2.4.10]: https://bitbucket.org/atlassian/atlassian-plugins-jquery/compare/release%2F2.2.4.10%0Drelease%2F2.2.4.9
[2.2.4.9]: https://bitbucket.org/atlassian/atlassian-plugins-jquery/compare/release%2F2.2.4.9%0Drelease%2F2.2.4.8
[2.2.4.8]: https://bitbucket.org/atlassian/atlassian-plugins-jquery/compare/release%2F2.2.4.8%0Drelease%2F2.2.4.7
[2.2.4.7]: https://bitbucket.org/atlassian/atlassian-plugins-jquery/compare/release%2F2.2.4.7%0Drelease%2F2.2.4.6
[2.2.4.6]: https://bitbucket.org/atlassian/atlassian-plugins-jquery/compare/release%2F2.2.4.6%0Drelease%2F2.2.4.5
[2.2.4.5]: https://bitbucket.org/atlassian/atlassian-plugins-jquery/compare/release%2F2.2.4.5%0Drelease%2F2.2.4.4
[2.2.4.4]: https://bitbucket.org/atlassian/atlassian-plugins-jquery/compare/release%2F2.2.4.4%0Drelease%2F2.2.4.3
[2.2.4.3]: https://bitbucket.org/atlassian/atlassian-plugins-jquery/compare/release%2F2.2.4.3%0Drelease%2F2.2.4.2
[2.2.4.2]: https://bitbucket.org/atlassian/atlassian-plugins-jquery/compare/release%2F2.2.4.2%0Drelease%2F2.2.4.1
[2.2.4.1]: https://bitbucket.org/atlassian/atlassian-plugins-jquery/compare/release%2F2.2.4.1%0Drelease%2F2.2.4

[CONFSRVDEV-15328]: https://jira.atlassian.com/browse/CONFSRVDEV-15328
[CONFSRVDEV-30703]: https://jira.atlassian.com/browse/CONFSRVDEV-30703

[CVE-2015-9251]: https://nvd.nist.gov/vuln/detail/cve-2015-9251
[CVE-2019-11358]: https://nvd.nist.gov/vuln/detail/CVE-2019-11358
[CVE-2020-11022]: https://nvd.nist.gov/vuln/detail/cve-2020-11022
[CVE-2020-11023]: https://nvd.nist.gov/vuln/detail/cve-2020-11023

[DCA11Y-861]: https://hello.jira.atlassian.cloud/browse/DCA11Y-861
 
[DCPL-1002]: https://ecosystem.atlassian.net/browse/DCPL-1002
 
[JFE-28]: https://bulldog.internal.atlassian.com/browse/JFE-28
 
[JQUERY-2]: https://ecosystem.atlassian.net/browse/JQUERY-2
[JQUERY-3]: https://ecosystem.atlassian.net/browse/JQUERY-3
[JQUERY-9]: https://ecosystem.atlassian.net/browse/JQUERY-9
[JQUERY-32]: https://ecosystem.atlassian.net/browse/JQUERY-32
 
[MNSTR-2847]: https://bulldog.internal.atlassian.com/browse/MNSTR-2847
 
[SNYK-JS-JQUERY-174006]: https://security.snyk.io/vuln/SNYK-JS-JQUERY-174006
[SNYK-JS-JQUERY-565129]: https://security.snyk.io/vuln/SNYK-JS-JQUERY-565129
[SNYK-JS-JQUERY-567880]: https://security.snyk.io/vuln/SNYK-JS-JQUERY-567880
[SNYK-JQUERY-20150627]: https://security.snyk.io/vuln/npm:jquery:20150627
 
[VULN-1051355]: https://asecurityteam.atlassian.net/browse/VULN-1051355
[VULN-1133278]: https://asecurityteam.atlassian.net/browse/VULN-1133278
[VULN-1133280]: https://asecurityteam.atlassian.net/browse/VULN-1133280
[VULN-1133281]: https://asecurityteam.atlassian.net/browse/VULN-1133281
