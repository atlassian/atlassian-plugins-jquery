// jest.config.js
// Sync object
/** @type {import('@jest/types').Config.InitialOptions} */
const config = {
    testMatch: ['<rootDir>/test/**/*-test.js'],
    testEnvironment: 'jsdom'
};

module.exports = config;
